﻿using System.Collections.Generic;

namespace Assets.Scripts
{
	public class ERecipe
	{
		#region Enumeration
		public enum TypeStatus
		{
			BadMixed, GoodMixed
		}

		#endregion

		#region Properties

		public int Dosage { get; set; }

		public Dictionary<string, EIngredient> Mix { get; set; }

		public TypeStatus Status { get; set; }

		#endregion
	}
}
