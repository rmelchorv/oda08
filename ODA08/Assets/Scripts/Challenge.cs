﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public abstract class Challenge : GameObject, IChallenge
	{
		#region Enumeration

		public enum Action
		{
			Ask, Inform, Leave, Pause, Restart, Resume
		}
		public enum Coroutine
		{
			
		}
		public enum Event
		{
			ClosePopUp, OpenPopUp
		}
		public enum State
		{
			Initial, Playing, Fighting, Finished
		}

		#endregion

		#region Attributes

		#region Protected

		protected bool EsAnimacionActiva;
		internal Oda08Indice Indice;
		protected Text Texto;
		protected string TextoAnimacion;
		protected float TiempoAnimacion, TiempoTotal;

		#endregion

		#endregion

		#region Constructor

		protected Challenge(string nombreReto) : base(nombreReto)
		{

		}

		#endregion

		#region Methods

		#region Protected

		internal abstract IEnumerator Execute(ESequence sequence, float waitingTime);
		protected abstract void ReStart();
		internal abstract void SetState(State state);

		#endregion

		#region Public

		/// <summary>
		/// Animates a text
		/// </summary>
		public void AnimateText()
		{
			if (!EsAnimacionActiva)
				return;

			TiempoAnimacion -= Time.deltaTime;

			if (TiempoAnimacion <= 0)
				TiempoAnimacion = 0;

			Texto.text = TextoAnimacion.Substring(0, (int)((TiempoTotal - TiempoAnimacion) * TextoAnimacion.Length / TiempoTotal));
		}

		/// <summary>
		/// Set a text to animate in a provided time
		/// </summary>
		/// <param name="text">Text to animate</param>
		/// <param name="time">Duration time of animation</param>
		public void SetTextToAnimate(string text, float time)
		{
			TextoAnimacion = text;
			TiempoTotal = TiempoAnimacion = time;
			EsAnimacionActiva = true;
		}

		#endregion

		#endregion
	}
}
