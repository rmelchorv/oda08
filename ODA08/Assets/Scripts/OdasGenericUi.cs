using JetBrains.Annotations;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class OdasGenericUi : MonoBehaviour
	{
		#region Attributes

		private Oda08Reto1 o08Reto1;
		private Oda08Reto2 o08Reto2;
		private Oda08Reto3 o08Reto3;
		private Oda08RetoFinal o08RetoFinal;
		private UnityEngine.GameObject imgBlockScreen, popUpInfo, popUp2Button;
		private Button btnAceptar, btnPausar, btnReiniciar, btnNo, btnSi;
		private Text txtInfo, txt2Button, txtTiempo;
		private Slider sldTimer;

		#endregion

		#region Events

		#region Private

		// Use this for initialization
		[UsedImplicitly]
		private void Start()
		{
			#region Object instances

			o08Reto1 = transform.GetComponent<Oda08Reto1>();
			o08Reto2 = transform.GetComponent<Oda08Reto2>();
			o08Reto3 = transform.GetComponent<Oda08Reto3>();
			o08RetoFinal = transform.GetComponent<Oda08RetoFinal>();

			SoundManager = transform.GetComponent<SoundManager>();

			GenericUi = transform.Find("genericUI").gameObject;
			GameUi = GenericUi.transform.Find("gameUI").gameObject;
			Counter = GameUi.transform.Find("contador").gameObject;

			imgBlockScreen = GenericUi.transform.Find("imgBlockScreen").gameObject;

			btnPausar = GameUi.transform.Find("btnPausa").GetComponent<Button>();
			btnReiniciar = GameUi.transform.Find("btnReiniciar").GetComponent<Button>();

			sldTimer = Counter.transform.Find("sldTimer").GetComponent<Slider>();

			popUpInfo = imgBlockScreen.transform.Find("popUpInfo").gameObject;
			popUp2Button = imgBlockScreen.transform.Find("popUp2Button").gameObject;

			txtInfo = popUpInfo.transform.Find("txtInfo").GetComponent<Text>();
			btnAceptar = popUpInfo.transform.Find("btnAceptar").GetComponent<Button>();

			txt2Button = popUp2Button.transform.Find("txt2Button").GetComponent<Text>();
			btnNo = popUp2Button.transform.Find("btnNo").GetComponent<Button>();
			btnSi = popUp2Button.transform.Find("btnSi").GetComponent<Button>();

			txtTiempo = Counter.transform.Find("txtTiempo").GetComponent<Text>();

			#endregion

			#region Event listeners

			btnPausar.onClick.AddListener(delegate
			{
				OpenPopUp(Challenge.Action.Pause, 0);
			});
			btnReiniciar.onClick.AddListener(delegate
			{
				OpenPopUp(Challenge.Action.Leave, -1);
			});

			#endregion

			SoundManager.PlaySound("audio-brebajes-y-pociones/fondo2", 0);
			Paused = true;
			Time = 11;
		}

		// Update is called once per frame
		[UsedImplicitly]
		private void Update()
		{
			if (txtTiempo.IsActive() && !Paused)
			{
				Time = Time - UnityEngine.Time.deltaTime;

				if (!(Time > 0.0f))
					ShowTimeoverWarning();
				else
				{
					txtTiempo.text = string.Format("{0:00}:{1:00}", (int)Time / 60, (int)Time % 60);
					sldTimer.value = Time;
				}
			}
		}

		private void ClosePopUp(Challenge.Action action)
		{
			switch (action)
			{
				#region Action cases

				case Challenge.Action.Ask:
					popUp2Button.gameObject.SetActive(false);
					break;
				case Challenge.Action.Inform:
					popUpInfo.gameObject.SetActive(false);
					break;
				case Challenge.Action.Pause:
					Paused = false;
					popUp2Button.gameObject.SetActive(false);
					break;

				#endregion
			}

			imgBlockScreen.SetActive(false);
			SoundManager.PlaySound("audios-basicos/cerrar3", 1);
		}

		private void InitializeUi()
		{
			ClosePopUp(Challenge.Action.Inform);
			ClosePopUp(Challenge.Action.Ask);

			txtTiempo.text = "05:00";
			GameUi.SetActive(false);
			Paused = true;
			Time = 301;
		}

		private void OpenPopUp(Challenge.Action action, int challengeNum)
		{
			imgBlockScreen.SetActive(true);

			switch (action)
			{
				#region Action cases

				case Challenge.Action.Ask:
					popUp2Button.gameObject.SetActive(true);
					break;
				case Challenge.Action.Inform:
					popUpInfo.gameObject.SetActive(true);
					break;
				case Challenge.Action.Leave:
					GoToMenu();
					break;
				case Challenge.Action.Pause:
					Paused = true;
					Show2ButtonPopUp(
						"\n<size=78>PAUSA</size>", new ESequence {
							Action = Challenge.Action.Leave,
							ChallengeNum = challengeNum,
							Event = Challenge.Event.ClosePopUp
						}, new ESequence {
							Action = Challenge.Action.Resume,
							Event = Challenge.Event.ClosePopUp
						}, "ABANDONAR", "CONTINUAR"
					);
					break;
				case Challenge.Action.Restart:
					Restart();
					break;
				case Challenge.Action.Resume:
					Resume();
					break;

					#endregion
			}

			SoundManager.PlaySound("audios-basicos/open-pop-up10", 1);
		}

		private void Restart()
		{
			InitializeUi();

			if (o08Reto1.IsActive())
			{
				GetComponent<Oda08Reto1>().SetState(Challenge.State.Initial);
				GetComponent<Oda08Indice>().MostrarReto("btnReto1");
			}
			else if (o08Reto2.IsActive())
			{
				GetComponent<Oda08Reto2>().SetState(Challenge.State.Initial);
				GetComponent<Oda08Indice>().MostrarReto("btnReto2");
			}
			else if (o08Reto3.IsActive())
			{
				GetComponent<Oda08Reto3>().SetState(Challenge.State.Initial);
				GetComponent<Oda08Indice>().MostrarReto("btnReto3");
			}
		}

		private void ShowTimeoverWarning()
		{
			#region Paused/Show warning

			//Paused = true;
			//Show2ButtonPopUp(
			//	'\u00BF' + "Quieres volver a intentarlo ahora?", new ESequence {
			//		Action = Challenge.Action.Leave,
			//		ChallengeNum = 0,
			//		Event = Challenge.Event.ClosePopUp
			//	}, new ESequence {
			//		Action = Challenge.Action.Restart,
			//		Event = Challenge.Event.ClosePopUp
			//	}, "NO", "S" + '\u00CD'
			//);

			#endregion

			if (o08Reto1.IsActive())
				GetComponent<Oda08Reto1>().SetState(Challenge.State.Fighting);
			else if (o08Reto2.IsActive())
				GetComponent<Oda08Reto2>().SetState(Challenge.State.Fighting);
			else if (o08Reto3.IsActive())
				GetComponent<Oda08Reto3>().SetState(Challenge.State.Fighting);
		}

		private IEnumerator UiSequence(ESequence sequence)
		{
			yield return new WaitForSeconds(0.01f);

			switch (sequence.Event)
			{
				case Challenge.Event.OpenPopUp:
					OpenPopUp(sequence.Action, sequence.ChallengeNum);
					break;
				case Challenge.Event.ClosePopUp:
					ClosePopUp(sequence.Action);
					break;
			}
		}

		#endregion

		#region Public

		/// <summary>
		/// Goes to the main menu
		/// </summary>
		public void GoToMenu()
		{
			InitializeUi();

			GetComponent<Oda08Indice>().Mostrar();
			GetComponent<Oda08Reto1>().SetState(Challenge.State.Initial);
			GetComponent<Oda08Reto2>().SetState(Challenge.State.Initial);
			GetComponent<Oda08Reto3>().SetState(Challenge.State.Initial);

			SoundManager.PlaySound("audio-brebajes-y-pociones/fondo2", 0);
		}

		/// <summary>
		/// Resume the current action and initiates the countdown
		/// </summary>
		public void Resume()
		{
			GameUi.SetActive(true);
			Paused = false;
		}

		/// <summary>
		/// Show a text in the Info-PopUp, in a specified time. Also, execute a sequence of action/event.
		/// </summary>
		/// <param name="text">The text to show</param>
		/// <param name="sequence">The sequence of action/event to execute</param>
		/// <param name="waitingTime">The waiting time to launch the PopUp</param>
		public void ShowInfoPopUp(string text, ESequence sequence, float waitingTime)
		{
			imgBlockScreen.SetActive(true);
			popUpInfo.SetActive(true);

			SoundManager.PlaySound("audios-basicos/open-pop-up10", 1);

			txtInfo.text = text;

			btnAceptar.onClick.RemoveAllListeners();

			switch (sequence.ChallengeNum)
			{
				#region Challenge cases

				case 0:
					btnAceptar.onClick.AddListener(delegate
					{
						StartCoroutine(UiSequence(sequence));
						ClosePopUp(Challenge.Action.Inform);
					});
					break;
				case 1:
					btnAceptar.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto1.Execute(sequence, waitingTime));
						ClosePopUp(Challenge.Action.Inform);
					});
					break;
				case 2:
					btnAceptar.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto2.Execute(sequence, waitingTime));
						ClosePopUp(Challenge.Action.Inform);
					});
					break;
				case 3:
					btnAceptar.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto3.Execute(sequence, waitingTime));
						ClosePopUp(Challenge.Action.Inform);
					});
					break;
				
				#endregion
			}
		}

		/// <summary>
		/// Show a text in the 2-Button-PopUp, in a specified time. Also, execute tow sequences of action/event.
		/// </summary>
		/// <param name="text">The text to show</param>
		/// <param name="firstSequence">The first sequence of action/event to execute</param>
		/// <param name="secondSequence">The second sequence of action/event to execute</param>
		/// <param name="textFirstButton">The text of first button</param>
		/// <param name="textSecondButton">The text of second button</param>
		public void Show2ButtonPopUp(string text, ESequence firstSequence, ESequence secondSequence, string textFirstButton, string textSecondButton)
		{
			imgBlockScreen.SetActive(true);
			popUp2Button.SetActive(true);

			txt2Button.text = text;

			btnNo.transform.GetChild(0).GetComponent<Text>().text = textFirstButton;
			btnSi.transform.GetChild(0).GetComponent<Text>().text = textSecondButton;
			btnNo.onClick.RemoveAllListeners();
			btnSi.onClick.RemoveAllListeners();

			switch (firstSequence.ChallengeNum)
			{
				#region Challenge cases

				case 0:
					btnNo.onClick.AddListener(delegate
					{
						OpenPopUp(firstSequence.Action, firstSequence.ChallengeNum);
						ClosePopUp(Challenge.Action.Ask);
					});
					btnSi.onClick.AddListener(delegate
					{
						OpenPopUp(secondSequence.Action, firstSequence.ChallengeNum);
						ClosePopUp(Challenge.Action.Ask);
					});
					break;
				case 1:
					btnNo.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto1.Execute(firstSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					btnSi.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto1.Execute(secondSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					break;
				case 2:
					btnNo.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto2.Execute(firstSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					btnSi.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto2.Execute(secondSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					break;
				case 3:
					btnNo.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto3.Execute(firstSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					btnSi.onClick.AddListener(delegate
					{
						StartCoroutine(o08Reto3.Execute(secondSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					break;
				case 4:
					btnNo.onClick.AddListener(delegate
					{
						StartCoroutine(o08RetoFinal.Execute(firstSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					btnSi.onClick.AddListener(delegate
					{
						StartCoroutine(o08RetoFinal.Execute(secondSequence, 1.0f));
						ClosePopUp(Challenge.Action.Ask);
					});
					break;

				#endregion
			}
		}

		#endregion

		#endregion

		#region Properties

		[HideInInspector]
		public UnityEngine.GameObject Counter { get; set; }
		[HideInInspector]
		public UnityEngine.GameObject GameUi { get; set; }
		[HideInInspector]
		public UnityEngine.GameObject GenericUi { get; set; }
		[HideInInspector]
		public bool Paused { get; set; }
		[HideInInspector]
		public SoundManager SoundManager { get; set; }
		[HideInInspector]
		public float Time { get; set; }

		#endregion
	}
}
