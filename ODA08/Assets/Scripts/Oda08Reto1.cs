﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class Oda08Reto1 : Challenge
	{
		#region Enumeration

		public new enum Action
		{
			ShowEnemy, ShowProfessor
		}
		public new enum Coroutine
		{
			FlaskAnimation, IngredientZoom
		}

		#endregion

		#region Attributes

		private Animator animPuerta, animZoom;
		private bool esRetoActivo;
		private Button btnPuerta, btnPocion1, btnPocion2, btnPocion3, btnPocion4, btnMezclar, btnAguaCactus, btnAguaManantial, btnAguaPozo, btnAguaTriangulo, btnAlasUnicornio, btnAureola, btnCorteza, btnEspina, btnLagrimas, btnLimadura, btnMaderaBarco, btnPlumaAveztruz, btnSalivaCaracol, btnSalivaTroll, btnTierraTunel, btnUniaCorrecaminos, btnUniaLeon, btnAceptarLiquidos, btnAceptarSolidos, btnBorrarSolidos, btnTeclado0, btnTeclado1, btnTeclado2, btnTeclado3, btnTeclado4, btnTeclado5, btnTeclado6, btnTeclado7, btnTeclado8, btnTeclado9, btnMezcla1, btnMezcla2, btnMezcla3, btnMezcla4;
		private UnityEngine.GameObject goInicio, goProfesor, goGloboProfesor, goLaboratorio, goEnemigo, goPociones, goReceta, goMezcla, goZonaPreparacion, goIngredientes, goZonaBloqueo, goMedidorLiquidos, goMedidorSolidos, goZonaAtaque, goZonaEnemigo, goZonaMezclas;
		private Image imgInterfaz, imgReceta, imgAreaInteractiva;
		private int numPocionActual, numIngredientesServidos, numPocionesMezcladas;
		private List<string> lsIngredientes;
		private List<ERecipe> loRecetas;
		private System.Random rndDosis, rndLiquidos, rndSolidos;
		private Slider sldMedidorLiquidos;
		private string ingredienteActual, proporcionSolido;
		private Text txtReceta, txtProporcionSolido;

		#endregion

		#region Constructor

		public Oda08Reto1() : base("Reto1")
		{
			
		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
		{
			#region Object instances

			lsIngredientes = new List<string> {
				"AguaCactus", "AguaManantial", "AguaPozo", "AguaTriangulo", "AlasUnicornio", "Aureola",
				"Corteza", "Espina", "Lagrimas", "Limadura", "MaderaBarco", "PlumaAveztruz", "SalivaCaracol",
				"SalivaTroll", "TierraTunel", "UniaCorrecaminos", "UniaLeon"
			};
			rndDosis = new System.Random();
			rndLiquidos = new System.Random();
			rndSolidos = new System.Random();

			goInicio = Canvas.transform.Find("Inicio").gameObject;
			goLaboratorio = Canvas.transform.Find("Laboratorio").gameObject;
			goZonaAtaque = Canvas.transform.Find("ZonaAtaque").gameObject;

			btnPuerta = goInicio.transform.Find("btnPuerta").GetComponent<Button>();
			goProfesor = goInicio.transform.Find("Profesor").gameObject;

			animPuerta = btnPuerta.GetComponent<Animator>();
			animPuerta.speed = 0f;

			goGloboProfesor = goProfesor.transform.Find("GloboProfesor").gameObject;

			goEnemigo = goLaboratorio.transform.Find("Enemigo").gameObject;
			goZonaPreparacion = goLaboratorio.transform.Find("ZonaPreparacion").gameObject;

			imgInterfaz = goEnemigo.transform.Find("imgInterfaz").GetComponent<Image>();

			goPociones = imgInterfaz.transform.Find("Pociones").gameObject;
			goReceta = imgInterfaz.transform.Find("Receta").gameObject;
			goMezcla = imgInterfaz.transform.Find("Mezcla").gameObject;

			btnPocion1 = goPociones.transform.Find("btnPocion1").GetComponent<Button>();
			btnPocion2 = goPociones.transform.Find("btnPocion2").GetComponent<Button>();
			btnPocion3 = goPociones.transform.Find("btnPocion3").GetComponent<Button>();
			btnPocion4 = goPociones.transform.Find("btnPocion4").GetComponent<Button>();

			imgReceta = goReceta.transform.Find("imgReceta").GetComponent<Image>();

			txtReceta = imgReceta.transform.Find("txtReceta").GetComponent<Text>();

			btnMezclar = goMezcla.transform.Find("btnMezclar").GetComponent<Button>();

			goIngredientes = goZonaPreparacion.transform.Find("Ingredientes").gameObject;
			goZonaBloqueo = goZonaPreparacion.transform.Find("ZonaBloqueo").gameObject;
			goMedidorLiquidos = goZonaPreparacion.transform.Find("MedidorLiquidos").gameObject;
			goMedidorSolidos = goZonaPreparacion.transform.Find("MedidorSolidos").gameObject;

			btnAguaCactus = goIngredientes.transform.Find("btnAguaCactus").GetComponent<Button>();
			btnAguaManantial = goIngredientes.transform.Find("btnAguaManantial").GetComponent<Button>();
			btnAguaPozo = goIngredientes.transform.Find("btnAguaPozo").GetComponent<Button>();
			btnAguaTriangulo = goIngredientes.transform.Find("btnAguaTriangulo").GetComponent<Button>();
			btnAlasUnicornio = goIngredientes.transform.Find("btnAlasUnicornio").GetComponent<Button>();
			btnAureola = goIngredientes.transform.Find("btnAureola").GetComponent<Button>();
			btnCorteza = goIngredientes.transform.Find("btnCorteza").GetComponent<Button>();
			btnEspina = goIngredientes.transform.Find("btnEspina").GetComponent<Button>();
			btnLagrimas = goIngredientes.transform.Find("btnLagrimas").GetComponent<Button>();
			btnLimadura = goIngredientes.transform.Find("btnLimadura").GetComponent<Button>();
			btnMaderaBarco = goIngredientes.transform.Find("btnMaderaBarco").GetComponent<Button>();
			btnPlumaAveztruz = goIngredientes.transform.Find("btnPlumaAveztruz").GetComponent<Button>();
			btnSalivaCaracol = goIngredientes.transform.Find("btnSalivaCaracol").GetComponent<Button>();
			btnSalivaTroll = goIngredientes.transform.Find("btnSalivaTroll").GetComponent<Button>();
			btnTierraTunel = goIngredientes.transform.Find("btnTierraTunel").GetComponent<Button>();
			btnUniaCorrecaminos = goIngredientes.transform.Find("btnUniaCorrecaminos").GetComponent<Button>();
			btnUniaLeon = goIngredientes.transform.Find("btnUniaLeon").GetComponent<Button>();

			foreach (string ingrediente in lsIngredientes)
			{
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
			}

			btnAceptarLiquidos = goMedidorLiquidos.transform.Find("btnAceptar").GetComponent<Button>();
			sldMedidorLiquidos = goMedidorLiquidos.transform.Find("imgProbeta").GetComponent<Image>().transform.Find("sldMedidor").GetComponent<Slider>();

			imgAreaInteractiva = sldMedidorLiquidos.GetComponentsInChildren<Image>().FirstOrDefault(item => item.name == "imgAreaInteractiva");

			txtProporcionSolido = goMedidorSolidos.transform.Find("imgBascula").GetComponent<Image>().transform.Find("txtBascula").GetComponent<Text>();
			btnAceptarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnAceptar").GetComponent<Button>();
			btnBorrarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnBorrar").GetComponent<Button>();
			btnTeclado0 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla0").GetComponent<Button>();
			btnTeclado1 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla1").GetComponent<Button>();
			btnTeclado2 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla2").GetComponent<Button>();
			btnTeclado3 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla3").GetComponent<Button>();
			btnTeclado4 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla4").GetComponent<Button>();
			btnTeclado5 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla5").GetComponent<Button>();
			btnTeclado6 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla6").GetComponent<Button>();
			btnTeclado7 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla7").GetComponent<Button>();
			btnTeclado8 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla8").GetComponent<Button>();
			btnTeclado9 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla9").GetComponent<Button>();

			goZonaEnemigo = goZonaAtaque.transform.Find("ZonaEnemigo").gameObject;
			goZonaMezclas = goZonaAtaque.transform.Find("ZonaMezclas").gameObject;

			goZonaMezclas.transform.Find("imgZoomInMatraz").GetComponent<Image>().enabled = false;
			goZonaMezclas.transform.Find("imgMatraz").GetComponent<Image>().enabled = false;
			goZonaMezclas.transform.Find("imgZoomOutMatraz").GetComponent<Image>().enabled = false;

			btnMezcla1 = goZonaMezclas.transform.Find("btnMezcla1").GetComponent<Button>();
			btnMezcla2 = goZonaMezclas.transform.Find("btnMezcla2").GetComponent<Button>();
			btnMezcla3 = goZonaMezclas.transform.Find("btnMezcla3").GetComponent<Button>();
			btnMezcla4 = goZonaMezclas.transform.Find("btnMezcla4").GetComponent<Button>();

			btnMezcla1.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla1.enabled = false;

			btnMezcla2.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla2.enabled = false;

			btnMezcla3.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla3.enabled = false;

			btnMezcla4.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla4.enabled = false;

			InicializarPociones();

			#endregion

			#region Event listeners

			btnPuerta.onClick.AddListener(MostrarLaboratorio);
			btnPocion1.onClick.AddListener(delegate
			{
				MostrarReceta(1);
				HabilitarZonaPreparacion();
			});
			btnPocion2.onClick.AddListener(delegate
			{
				MostrarReceta(2);
				HabilitarZonaPreparacion();
			});
			btnPocion3.onClick.AddListener(delegate
			{
				MostrarReceta(3);
				HabilitarZonaPreparacion();
			});
			btnPocion4.onClick.AddListener(delegate
			{
				MostrarReceta(4);
				HabilitarZonaPreparacion();
			});
			btnMezclar.onClick.AddListener(MezclarIngredientes);
			btnAguaCactus.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "AguaCactus");
			});
			btnAguaManantial.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "AguaManantial");
			});
			btnAguaPozo.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "AguaPozo");
			});
			btnAguaTriangulo.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "AguaTriangulo");
			});
			btnAlasUnicornio.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "AlasUnicornio");
			});
			btnAureola.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "Aureola");
			});
			btnCorteza.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "Corteza");
			});
			btnEspina.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "Espina");
			});
			btnLagrimas.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "Lagrimas");
			});
			btnLimadura.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "Limadura");
			});
			btnMaderaBarco.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "MaderaBarco");
			});
			btnPlumaAveztruz.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "PlumaAveztruz");
			});
			btnSalivaCaracol.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "SalivaCaracol");
			});
			btnSalivaTroll.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "SalivaTroll");
			});
			btnTierraTunel.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "TierraTunel");
			});
			btnUniaCorrecaminos.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "UniaCorrecaminos");
			});
			btnUniaLeon.onClick.AddListener(delegate
			{
				IniciarCorrutina(Coroutine.IngredientZoom, "UniaLeon");
			});
			btnAceptarLiquidos.onClick.AddListener(GuardarProporcion);
			btnAceptarSolidos.onClick.AddListener(GuardarProporcion);
			btnBorrarSolidos.onClick.AddListener(BorrarProporcion);
			btnTeclado0.onClick.AddListener(delegate
			{
				SumarProporcion(0);
			});
			btnTeclado1.onClick.AddListener(delegate
			{
				SumarProporcion(1);
			});
			btnTeclado2.onClick.AddListener(delegate
			{
				SumarProporcion(2);
			});
			btnTeclado3.onClick.AddListener(delegate
			{
				SumarProporcion(3);
			});
			btnTeclado4.onClick.AddListener(delegate
			{
				SumarProporcion(4);
			});
			btnTeclado5.onClick.AddListener(delegate
			{
				SumarProporcion(5);
			});
			btnTeclado6.onClick.AddListener(delegate
			{
				SumarProporcion(6);
			});
			btnTeclado7.onClick.AddListener(delegate
			{
				SumarProporcion(7);
			});
			btnTeclado8.onClick.AddListener(delegate
			{
				SumarProporcion(8);
			});
			btnTeclado9.onClick.AddListener(delegate
			{
				SumarProporcion(9);
			});
			btnMezcla1.onClick.AddListener(delegate
			{
				LanzarAtaque(btnMezcla1.transform.Find("txtMezcla").GetComponent<Text>().text);
			});
			btnMezcla2.onClick.AddListener(delegate
			{
				LanzarAtaque(btnMezcla2.transform.Find("txtMezcla").GetComponent<Text>().text);
			});
			btnMezcla3.onClick.AddListener(delegate
			{
				LanzarAtaque(btnMezcla3.transform.Find("txtMezcla").GetComponent<Text>().text);
			});
			btnMezcla4.onClick.AddListener(delegate
			{
				LanzarAtaque(btnMezcla4.transform.Find("txtMezcla").GetComponent<Text>().text);
			});

			#endregion
		}

		// Update is called once per frame
		protected override void Update()
		{
			#region Initial state

			if (Instance.activeInHierarchy)
			{
				if (!esRetoActivo)
				{
					GenericUi.ShowInfoPopUp(
						"¡La escuela de magia Kadabra, se alegra de tenerte entre sus estudiantes! Deberás esforzarte porque el profesor Galaguer es uno de los más exigentes",
						new ESequence {
							Action = (Challenge.Action) Action.ShowProfessor,
							ChallengeNum = 1,
							Event = Event.ClosePopUp
						}, 0.0f
					);
					esRetoActivo = true;
				}
			}
			else
				esRetoActivo = false;

			#endregion

			AnimateText();
		}

		#endregion

		#region Private

		private void ClosePopUp(Action action)
		{
			switch (action)
			{
				#region Cases

				case Action.ShowEnemy:
					goEnemigo.SetActive(true);
					break;
				case Action.ShowProfessor:
					goProfesor.SetActive(true);
					animPuerta.Play("Puerta");
					animPuerta.speed = 1f;
					Invoke("MostrarGloboProfesor", 0.45f);
					break;

				#endregion
			}

			GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
		}

		#endregion

		#endregion

		#region Methods

		#region Private

		private void BorrarProporcion()
		{
			proporcionSolido = string.Empty;
			txtProporcionSolido.text = "0";
			GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/click", 1);
		}

		private void ContabilizarIngredienteServido()
		{
			goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
			goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
			GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/click", 1);

			#region Servir ingrediente

			if (loRecetas[numPocionActual - 1].Mix[ingredienteActual].Status == EIngredient.TypeStatus.NotServed)
			{
				loRecetas[numPocionActual - 1].Mix[ingredienteActual].Status = EIngredient.TypeStatus.Served;
				numIngredientesServidos++;
			}

			#endregion

			if (numIngredientesServidos == loRecetas[numPocionActual - 1].Mix.Count)
			  //goMezcla.SetActive(true);
				IniciarCorrutina(Coroutine.FlaskAnimation);
		}

		private void DisminuirVidaEnemigo()
		{
			
		}

		private void EvaluarMezcla()
		{
			bool esMezclaCorrecta = true;

			#region Evaluar las proporciones de los ingredientes

			foreach (EIngredient ingrediente in loRecetas[numPocionActual - 1].Mix.Values)
			{
				if (loRecetas[numPocionActual - 1].Dosage * ingrediente.Quantity == ingrediente.Proportion)
					continue;

				esMezclaCorrecta = false;
				break;
			}

			numPocionesMezcladas++;
			goZonaMezclas.transform.Find("btnMezcla" + numPocionesMezcladas).GetComponent<Button>().transform.Find("txtMezcla").GetComponent<Text>().text = (numPocionActual - 1).ToString();
			goZonaMezclas.transform.Find("btnMezcla" + numPocionesMezcladas).GetComponent<Button>().enabled = true;

			#endregion

			if (esMezclaCorrecta)
			{
				goZonaMezclas.transform.Find("btnMezcla" + numPocionesMezcladas).GetComponent<Button>().image.sprite = Resources.Load<Sprite>("reto1/frasco-azul");
				loRecetas[numPocionActual - 1].Status = ERecipe.TypeStatus.GoodMixed;
			}
		}

		private int GenerarCantidadIngrediente(EIngredient.TypeIngredient tipoIngrediente)
		{
			int cantidad = 0;

			switch (tipoIngrediente)
			{
				case EIngredient.TypeIngredient.Liquid:
					cantidad = rndLiquidos.Next(5, 26);
					break;
				case EIngredient.TypeIngredient.Solid:
					cantidad = rndSolidos.Next(5, 251);
					break;
			}

			return cantidad - cantidad % 5;
		}

		private void GuardarProporcion()
		{
			if (numPocionActual < 1 || string.IsNullOrEmpty(ingredienteActual))
			{
				goMedidorLiquidos.SetActive(false);
				goMedidorSolidos.SetActive(false);
			}
			else
				if (loRecetas[numPocionActual - 1].Mix[ingredienteActual].Type == EIngredient.TypeIngredient.Liquid)
				{
					#region Guardar proporción de ingrediente líquido
				
					if ((int)sldMedidorLiquidos.value > 0)
					{
						int cantidad = Mathf.RoundToInt(sldMedidorLiquidos.value);
						int residuo = cantidad % 10;

						cantidad -= residuo;

						if (residuo > 7)
							cantidad += 10;
						else if (residuo > 2)
							cantidad += 5;

						loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = cantidad;
						sldMedidorLiquidos.value = 0;
						ContabilizarIngredienteServido();
					}

					goMedidorLiquidos.SetActive(false);

					#endregion
				}
				else
				{
					#region Guardar proporción de ingrediente sólido

					if (!string.IsNullOrEmpty(proporcionSolido))
					{
						int proporcion = int.Parse(proporcionSolido);

						if (proporcion != 0)
						{
							loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = proporcion;
							ContabilizarIngredienteServido();
						}

						proporcionSolido = string.Empty;
						txtProporcionSolido.text = "0";
					}

					goMedidorSolidos.SetActive(false);

					#endregion
				}
		}

		private void HabilitarZonaPreparacion()
		{
			goZonaBloqueo.SetActive(false);
			GenericUi.Resume();
		}

		private void IniciarCorrutina(Coroutine nombreCorrutina, params object[] parametros)
		{
			switch (nombreCorrutina)
			{
				case Coroutine.FlaskAnimation:
					StartCoroutine("MostrarZonaMezcla");
					break;
				case Coroutine.IngredientZoom:
					StartCoroutine("ZoomIngrediente", parametros[0].ToString());
					break;
			}
		}

		private void InicializarPociones()
		{
			//ReSharper disable once UseObjectOrCollectionInitializer
			loRecetas = new List<ERecipe>();

			#region Inicializar ingredientes de pociones

			loRecetas.Add(new ERecipe
			{
				Dosage = rndDosis.Next(1, 5),
				Mix = new Dictionary<string, EIngredient>
				{
					{"Corteza", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"AguaCactus", new EIngredient {Color = new Color32(0x79, 0xD0, 0xD3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
					{"PlumaAveztruz", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"SalivaCaracol", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
					{"Aureola", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}}
				}

			});
			loRecetas.Add(new ERecipe
			{
				Dosage = rndDosis.Next(1, 5),
				Mix = new Dictionary<string, EIngredient>
				{
					{"Limadura", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"UniaLeon", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"AguaManantial", new EIngredient {Color = new Color32(0x04, 0xF0, 0xFF, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
					{"SalivaTroll", new EIngredient {Color = new Color32(0x00, 0xED, 0x00, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}}
				}

			});
			loRecetas.Add(new ERecipe
			{
				Dosage = rndDosis.Next(1, 5),
				Mix = new Dictionary<string, EIngredient>
				{
					{"Espina", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"Corteza", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"Lagrimas", new EIngredient {Color = new Color32(0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
					{"AguaPozo", new EIngredient {Color = new Color32(0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
					{"TierraTunel", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}}
				}

			});
			loRecetas.Add(new ERecipe
			{
				Dosage = rndDosis.Next(1, 5),
				Mix = new Dictionary<string, EIngredient>
				{
					{"UniaCorrecaminos", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"AlasUnicornio", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
					{"AguaTriangulo", new EIngredient {Color = new Color32(0x33, 0xAF, 0xCF, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
					{"MaderaBarco", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}}
				}

			});

			#endregion
		}

		private void LanzarAtaque(string pocion)
		{
			int numPocion;

			if (int.TryParse(pocion, out numPocion))
				DisminuirVidaEnemigo();
			else
				MostrarAtaqueEnemigo();
		}

		private void MezclarIngredientes()
		{
			GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/click", 1);

			goMezcla.SetActive(false);
		  //IniciarCorrutina(Coroutine.FlaskAnimation);
		}

		[UsedImplicitly]
		private void MostrarGloboProfesor()
		{
			goGloboProfesor.SetActive(true);
			Texto = goGloboProfesor.transform.Find("txtGloboProfesor").GetComponent<Text>();
			SetTextToAnimate("Toca la puerta para iniciar el curso de Brebajes y pociones.", 4.0f);
		}

		private void MostrarLaboratorio()
		{

			goLaboratorio.SetActive(true);
			goInicio.SetActive(false);

			GenericUi.ShowInfoPopUp(
				"Prueba 1: vencer al Grifo.\nSelecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones.",
				new ESequence {
					Action = (Challenge.Action)Action.ShowEnemy,
					ChallengeNum = 1,
					Event = Event.ClosePopUp
				}, 0.0f
			);
		}

		private void MostrarAtaqueEnemigo()
		{
			
		}

		private void MostrarPocionesRestantes()
		{
			goReceta.SetActive(false);
			goPociones.SetActive(true);
			goZonaBloqueo.SetActive(true);

			#region Reinicializar componentes

			Color alpha = goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().image.color;

			alpha.a = 0.70f;
			goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;
			goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().image.color = alpha;

			ingredienteActual = string.Empty;
			numIngredientesServidos = 0;
			numPocionActual = 0;

			#endregion
		}

		private void MostrarReceta(int numPocion)
		{
			numPocionActual = numPocion;
			goPociones.SetActive(false);
			goReceta.SetActive(true);

			switch (numPocion)
			{
				#region Recetas

				case 1:
					txtReceta.text = 
						"<size=24><b>APTERA:</b></size> Evita volar.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	" + loRecetas[0].Mix["Corteza"].Quantity + " gr de corteza de árbol llorón\n" +
						"	" + loRecetas[0].Mix["AguaCactus"].Quantity + " ml de agua de cactus\n" +
						"	" + loRecetas[0].Mix["PlumaAveztruz"].Quantity + " gr de plumas de avestruz\n" +
						"	" + loRecetas[0].Mix["SalivaCaracol"].Quantity + " ml de saliva de caracol\n" +
						"	" + loRecetas[0].Mix["Aureola"].Quantity + " gr de aureola de ángel caído\n\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[0].Dosage + " DOSIS.</b>";
					break;
				case 2:
					txtReceta.text = 
						"<size=24><b>UNGUIBUS:</b></size> Evita que use sus garras.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	" + loRecetas[1].Mix["Limadura"].Quantity + " gr de lija celestial\n" +
						"	" + loRecetas[1].Mix["UniaLeon"].Quantity + " gr de uña molida de león\n" +
						"	" + loRecetas[1].Mix["AguaManantial"].Quantity + " ml de agua del gran manantial\n" +
						"	" + loRecetas[1].Mix["SalivaTroll"].Quantity + " ml de saliva ácida de troll\n\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[1].Dosage + " DOSIS.</b>";
					break;
				case 3:
					txtReceta.text = 
						"<size=24><b>CAVAEM:</b></size> Encierra en una prisión invisible.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	" + loRecetas[2].Mix["Espina"].Quantity + " gr de espinas de puercoespín\n" +
						"	" + loRecetas[2].Mix["Corteza"].Quantity + " gr de corteza de árbol llorón\n" +
						"	" + loRecetas[2].Mix["Lagrimas"].Quantity + " ml de lágrimas del eterno prisionero\n" +
						"	" + loRecetas[2].Mix["AguaPozo"].Quantity + " ml de agua del pozo oscuro\n" +
						"	" + loRecetas[2].Mix["TierraTunel"].Quantity + " gr de tierra del túnel sin salida\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[2].Dosage + " DOSIS.</b>";
					break;
				case 4:
					txtReceta.text = 
						"<size=24><b>TRANSPORT:</b></size> Desaparece al enemigo y lo hace reaparecer en el lugar que pienses.\n" +
						"Para <b>una</b> dosis:\n" +
						"	<size=20>" + loRecetas[3].Mix["UniaCorrecaminos"].Quantity + " gr de uña molida de correcaminos\n" +
						"	" + loRecetas[3].Mix["AlasUnicornio"].Quantity + " gr de alas de unicornio\n" +
						"	" + loRecetas[3].Mix["AguaTriangulo"].Quantity + " ml de agua del triángulo de las Bermudas\n" +
						"	" + loRecetas[3].Mix["MaderaBarco"].Quantity + " gr de madera del barco de Barba Negra</size>\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[3].Dosage + " DOSIS.</b>";
					break;

				#endregion
			}

			#region Mostrar ingredientes

			foreach (string ingrediente in loRecetas[numPocionActual - 1].Mix.Keys)
			{
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = true;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = true;
			}

			GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/click", 1);

			#endregion
		}

		[UsedImplicitly]
		private IEnumerator MostrarZonaMezcla()
		{
			goZonaMezclas.SetActive(true);

			#region Zoom-In a matraz

			goZonaMezclas.transform.Find("imgZoomInMatraz").GetComponent<Image>().enabled = true;

			Animator animMatraz = goZonaMezclas.transform.Find("imgZoomInMatraz").GetComponent<Image>().GetComponent<Animator>();

			animMatraz.Play("ZoomInMatraz", -1, 0f);
			animMatraz.speed = 1f;

			#endregion

			yield return new WaitForSeconds(animMatraz.GetCurrentAnimatorClipInfo(0)[0].clip.length + 0.05f);

			#region Animar matraz

			goZonaMezclas.transform.Find("imgZoomInMatraz").GetComponent<Image>().enabled = false;
			goZonaMezclas.transform.Find("imgMatraz").GetComponent<Image>().enabled = true;
			GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/agua3", 1);
			animMatraz = goZonaMezclas.transform.Find("imgMatraz").GetComponent<Image>().GetComponent<Animator>();
			animMatraz.Play("Matraz", -1, 0f);
			animMatraz.speed = 1f;

			#endregion

			yield return new WaitForSeconds(animMatraz.GetCurrentAnimatorClipInfo(0)[0].clip.length + 0.05f);

			#region Zoom-Out a matraz

			goZonaMezclas.transform.Find("imgMatraz").GetComponent<Image>().enabled = false;
			goZonaMezclas.transform.Find("imgZoomOutMatraz").GetComponent<Image>().enabled = true;
			animMatraz = goZonaMezclas.transform.Find("imgZoomOutMatraz").GetComponent<Image>().GetComponent<Animator>();
			animMatraz.Play("ZoomOutMatraz", -1, 0f);
			animMatraz.speed = 1f;

			#endregion

			yield return new WaitForSeconds(animMatraz.GetCurrentAnimatorClipInfo(0)[0].clip.length + 0.05f);

			#region Mezclar ingredientes

			goZonaMezclas.transform.Find("imgZoomOutMatraz").GetComponent<Image>().enabled = false;
			EvaluarMezcla();
			MostrarPocionesRestantes();

			#endregion

			if (numPocionesMezcladas == loRecetas.Count)
			{
				yield return new WaitForSeconds(0.5f);
				MostrarZonaAtaque(false);
			}
		}

		private void MostrarZonaAtaque(bool esEventoTimeover)
		{
			if (esEventoTimeover)
			{
				#region Habilitar mezclas

				btnMezcla1.enabled = true;
				btnMezcla2.enabled = true;
				btnMezcla3.enabled = true;
				btnMezcla4.enabled = true;
				goZonaMezclas.SetActive(true);

				#endregion
			}

			#region Mostrar animación mezclas

			btnMezcla1.transform.Find("imgMezcla").GetComponent<Image>().enabled = true;
			btnMezcla2.transform.Find("imgMezcla").GetComponent<Image>().enabled = true;
			btnMezcla3.transform.Find("imgMezcla").GetComponent<Image>().enabled = true;
			btnMezcla4.transform.Find("imgMezcla").GetComponent<Image>().enabled = true;

			#endregion

			goZonaEnemigo.SetActive(true);
			goLaboratorio.SetActive(false);
			GenericUi.GameUi.SetActive(false);
		}

		private void SumarProporcion(int cantidad)
		{
			#region Validar entrada de teclado

			if (cantidad == 0)
			{
				if (txtProporcionSolido.text == "0")
					return;
				if (txtProporcionSolido.text.Length == 3 && txtProporcionSolido.text != "100")
					return;
				if (txtProporcionSolido.text.Length > 3)
					return;
			}
			else
				if (txtProporcionSolido.text.Length > 2)
					return;

			#endregion

			GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/click", 1);
			proporcionSolido += cantidad;
			txtProporcionSolido.text = proporcionSolido;
		}

		[UsedImplicitly]
		private IEnumerator ZoomIngrediente(string ingrediente)
		{
			if (numPocionActual > 0 && loRecetas[numPocionActual - 1].Mix.ContainsKey(ingrediente))
			{
				#region Zoom-In a ingrediente

				ingredienteActual = ingrediente;
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = true;
				animZoom = goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>();
				animZoom.Play(ingrediente, -1, 0f);
				animZoom.speed = 1f;
				GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/zoom", 1);

				#endregion

				yield return new WaitForSeconds(animZoom.GetCurrentAnimatorClipInfo(0)[0].clip.length + 0.05f);

				#region Zoom-Out a ingrediente

				if (loRecetas[numPocionActual - 1].Mix[ingrediente].Type == EIngredient.TypeIngredient.Liquid)
				{
					imgAreaInteractiva.color = loRecetas[numPocionActual - 1].Mix[ingrediente].Color;
					goMedidorLiquidos.SetActive(true);
				}
				else
					goMedidorSolidos.SetActive(true);

				#endregion
			}
		}

		#endregion

		#region Protected

		/// <summary>
		/// Execute a serie of action/event of the sequence, after an specified time
		/// </summary>
		/// <param name="sequence">The action/event to execute</param>
		/// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
		/// <returns></returns>
		internal override IEnumerator Execute(ESequence sequence, float waitingTime)
		{
			yield return new WaitForSeconds(waitingTime);

			switch (sequence.Event)
			{
				case Event.ClosePopUp:
					ClosePopUp((Action)sequence.Action);
					break;
			}
		}

		/// <summary>
		/// Set the challange to its initial state
		/// </summary>
		protected override void ReStart()
		{
			#region Reset objects to initial state

			btnMezcla1.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla2.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla3.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla4.transform.Find("imgMezcla").GetComponent<Image>().enabled = false;
			btnMezcla1.transform.Find("txtMezcla").GetComponent<Image>().enabled = false;
			btnMezcla2.transform.Find("txtMezcla").GetComponent<Image>().enabled = false;
			btnMezcla3.transform.Find("txtMezcla").GetComponent<Image>().enabled = false;
			btnMezcla4.transform.Find("txtMezcla").GetComponent<Image>().enabled = false;
			btnMezcla1.enabled = false;
			btnMezcla2.enabled = false;
			btnMezcla3.enabled = false;
			btnMezcla4.enabled = false;

			foreach (string ingrediente in lsIngredientes)
			{
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
			}

			for (int i = 1; i < 5; i++)
			{
				Color alpha = goPociones.transform.Find("btnPocion" + i).GetComponent<Button>().image.color;

				alpha.a = 1.0f;
				goPociones.transform.Find("btnPocion" + i).GetComponent<Button>().enabled = true;
				goPociones.transform.Find("btnPocion" + i).GetComponent<Button>().image.color = alpha;
			}

			txtProporcionSolido.text = "0";
			txtReceta.text = string.Empty;

			goZonaMezclas.SetActive(false);
			goZonaEnemigo.SetActive(false);
			goMedidorLiquidos.SetActive(false);
			goMedidorSolidos.SetActive(false);
			goZonaBloqueo.SetActive(true);
			goMezcla.SetActive(false);
			goReceta.SetActive(false);
			goPociones.SetActive(true);
			goLaboratorio.SetActive(false);
			goGloboProfesor.SetActive(false);
			goProfesor.SetActive(false);
			goInicio.SetActive(true);

			animPuerta.speed = 0f;

			esRetoActivo = false;
			sldMedidorLiquidos.value = 0;
			numIngredientesServidos = 0;
			numPocionActual = 0;
			numPocionesMezcladas = 0;
			ingredienteActual = string.Empty;
			proporcionSolido = string.Empty;
			InicializarPociones();

			#endregion
		}

		/// <summary>
		/// Set the state of the sequence
		/// </summary>
		/// <param name="state">The new state of the sequence</param>
		internal override void SetState(State state)
		{
			switch (state)
			{
				case State.Initial:
					ReStart();
					break;
				case State.Playing:
					break;
				case State.Fighting:
					MostrarZonaAtaque(true);
					break;
				case State.Finished:
					break;
			}
		}

		#endregion

		#endregion
	}
}
