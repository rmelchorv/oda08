﻿using UnityEngine;

namespace Assets.Scripts
{
	public class EIngredient
	{
		#region Enumeration
		public enum TypeIngredient
		{
			Liquid, Solid
		}
		public enum TypeStatus
		{
			NotServed, Served
		}

		#endregion

		#region Properties

		public Color Color { get; set; }

		public int Proportion { get; set; }

		public int Quantity { get; set; }

		public TypeStatus Status { get; set; }

		public TypeIngredient Type { get; set; }

		#endregion
	}
}
