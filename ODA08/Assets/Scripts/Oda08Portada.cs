﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class Oda08Portada : MonoBehaviour
	{
		#region Attributes

		private UnityEngine.GameObject goPortada, goIndice;
		private Canvas cvPortada;
		private Button btnEntrar;

		#endregion

		#region Events

		#region Private

		// Use this for initialization
		[UsedImplicitly]
		private void Start()
		{
			//Set object instances
			goPortada = transform.Find("Portada").gameObject;
			goIndice = transform.Find("Indice").gameObject;

			cvPortada = goPortada.transform.Find("LienzoPortada").GetComponent<Canvas>();
			cvPortada.worldCamera = Camera.main;

			btnEntrar = cvPortada.transform.Find("btnEntrar").GetComponent<Button>();

			// Set listeners for events
			btnEntrar.onClick.AddListener(MostrarIndice);
		}

		private void MostrarIndice()
		{
			goPortada.SetActive(false);
			goIndice.SetActive(true);
		}

		#endregion

		#endregion
	}
}
