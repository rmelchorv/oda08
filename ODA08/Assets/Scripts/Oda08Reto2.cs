﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
	public class Oda08Reto2 : Challenge
	{
		#region Attributes



		#endregion

		#region Constructor

		public Oda08Reto2() : base("Reto2")
		{

		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
		{
			#region Object instances



			#endregion
		}

		// Update is called once per frame
		protected override void Update()
		{

		}

		#endregion

		#endregion

		#region Methods

		#region Protected

		internal override IEnumerator Execute(ESequence sequence, float waitingTime)
		{
			yield return new WaitForSeconds(waitingTime);


		}

		protected override void ReStart()
		{
			
		}

		internal override void SetState(State state)
		{
			
		}

		#endregion

		#endregion
	}
}
