﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class DropDownMenu : MonoBehaviour
	{
		#region Attributes

		private OdasGenericUi oguiGenericUi;
		private Oda08Indice o08Indice;
		private UnityEngine.GameObject goDropDownMenu, goFichaTecnica, goCreditos, goGlosario;
		private Button btnToggleMenu, btnClosePopUp, btnFichaTecnica, btnCreditos, btnGlosario, btnInicio;
		private Animator animMenu;
		private bool isMenuOpen;

		#endregion

		#region Events

		#region Private

		// Use this for initialization
		[UsedImplicitly]
		private void Start()
		{
			#region Object instances

			oguiGenericUi = transform.GetComponent<OdasGenericUi>();
			o08Indice = transform.GetComponent<Oda08Indice>();

			goDropDownMenu = transform.Find("genericUI").Find("dropDownMenu").gameObject;

			btnToggleMenu = goDropDownMenu.transform.GetChild(0).GetComponent<Button>();
			animMenu = goDropDownMenu.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
			btnClosePopUp = goDropDownMenu.transform.GetChild(1).GetComponent<Button>();

			btnFichaTecnica = animMenu.transform.Find("btnFichaTecnica").GetComponent<Button>();
			btnCreditos = animMenu.transform.Find("btnCreditos").GetComponent<Button>();
			btnGlosario = animMenu.transform.Find("btnGlosario").GetComponent<Button>();
			btnInicio = animMenu.transform.Find("btnInicio").GetComponent<Button>();

			goFichaTecnica = btnClosePopUp.transform.GetChild(0).GetChild(0).gameObject;
			goCreditos = btnClosePopUp.transform.GetChild(0).GetChild(1).gameObject;
			goGlosario = btnClosePopUp.transform.GetChild(0).GetChild(2).gameObject;

			#endregion

			#region Event listeners

			btnToggleMenu.onClick.AddListener(ToggleMenu);
			btnClosePopUp.onClick.AddListener(ClosePopUp);
			btnFichaTecnica.onClick.AddListener(delegate
			{
				OpenPopUp("FichaTecnica");
			});
			btnCreditos.onClick.AddListener(delegate
			{
				OpenPopUp("Creditos");
			});
			btnGlosario.onClick.AddListener(delegate
			{
				OpenPopUp("Glosario");
			});
			btnInicio.onClick.AddListener(GoToInicio);

			#endregion
		}

		private void ClosePopUp()
		{
			goFichaTecnica.SetActive(false);
			goCreditos.SetActive(false);
			goGlosario.SetActive(false);
			btnClosePopUp.gameObject.SetActive(false);
		}

		private void GoToInicio()
		{
			oguiGenericUi.GoToMenu();
			ToggleMenu();
		}

		private void OpenPopUp(string popUpName)
		{
			btnClosePopUp.gameObject.SetActive(true);

			switch (popUpName)
			{
				#region PopUp cases

				case "FichaTecnica":
					goFichaTecnica.SetActive(true);
					break;
				case "Creditos":
					goCreditos.SetActive(true);
					break;
				case "Glosario":
					goGlosario.SetActive(true);
					break;

				#endregion
			}
		}

		private void ToggleMenu()
		{
			isMenuOpen = !isMenuOpen;
			animMenu.SetBool("Opened", isMenuOpen);
		}

		// Update is called once per frame
		[UsedImplicitly]
		private void Update()
		{
			btnInicio.gameObject.SetActive(o08Indice.EsRetoActivo());
		}

		#endregion

		#endregion
	}
}
